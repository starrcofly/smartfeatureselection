# README #

This R package is about smart Feature Selection on biomedical data, with support of Greedy Ensemble and most caret Machine Learning algorithms.

Install:

if (!requireNamespace("devtools", quietly = TRUE))
    install.packages("devtools")
devtools::install_bitbucket("starrcofly/smartfeatureselection")
	
Test:

tests/test.R can be used to test the package after installing and also an examples of showing how to use the functions in this package. 

