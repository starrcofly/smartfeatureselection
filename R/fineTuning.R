fineTuning <- function (#
    in_rdata = NULL, 
    out_rdata = NULL, #is.null out_rdata, will return model
	X = NULL, #if is.null in_rdata, X, Y has to be set
	Y = NULL, #if is.null in_rdata, X, Y has to be set
    method,
	ctrl_method = "repeatedcv", #for trainControl
    tuneLength = 4,
    repeats = 2,
    cvnumber = 10, # folds number for cross validation
    val.index.list.f = NULL,
    ensemble.ml.algorithm,
    savePredictions = "final",
    nnetweight.factor = 10,
    metric = "ROC",
    # metric, "logLoss", "RMSE", ...
    pp = c("center", "scale"), #preProc / preProcess parameter, NULL, center, scale ...
    continue_on_fail = T,
    #for caretList
    mysummaryFunction = twoClassSummary,
    #twoClassSummary or multiClassSummary
	classProbs = T,
	casecontrol = T,
	CPUnum = 20,
	seed = 1492,
	allowParallel = T,
	#clParallelMode = F, #means already registered cluster, no need to re-register
	...)
{
	#library(caret)
	#library(dplyr) # Used by caret
	#library(kernlab)
	#library(doMC)
	if (allowParallel) {
		registerDoMC(CPUnum)
		registerDoParallel(CPUnum)
	}
	if (is.null(in_rdata)) {
		stopifnot(!is.null(X) & !is.null(Y))
	} else {
		load(in_rdata)
	}
	if (!is.data.frame(X)) X = as.data.frame(X)

	#check response error
	if (casecontrol) {
		y.table = table(Y)
		if (sum(y.table <= 1) > 0) {
			e.i = which(y.table <= 1)
			if (length(e.i) > 1) {
				e.y.i = c()
				for (ee in e.i) {
					e = names(y.table)[ee]
					e.y.i = c(e.y.i, which(Y == e))
				}
			} else {
				e = names(y.table)[e.i]
				e.y.i = which(Y == e)
			}
			Y = Y[-e.y.i]
			Y = factor(Y)
			X = X[-e.y.i,]
		}
	}

	MaxNWts = as.numeric(nnetweight.factor * (ncol(X) + 1) + nnetweight.factor + 1)
	set.seed(seed)

	###if (!casecontrol & metric != "ROC" ) {
	#if (!casecontrol) {
	#	metric = "Accuracy"
	#classProbs = F
	#}

	if (is.null(val.index.list.f)) {
		if (ctrl_method == "none") {
			ctrl <- trainControl(
								 method = ctrl_method
								 )
			#message (ctrl)
		} else {
			ctrl <-
				trainControl(
							 allowParallel = allowParallel,
							 method = ctrl_method,
							 number = cvnumber,
							 repeats = repeats,
							 summaryFunction = mysummaryFunction,
							 classProbs = classProbs,
							 savePredictions = savePredictions
							 )
		}
	} else {
		load(val.index.list.f)
		ctrl <-
			trainControl(
						 allowParallel = allowParallel,
						 method = "boot",
						 number = length(val.index.list),
						 summaryFunction = mysummaryFunction,
						 index = val.index.list,
						 classProbs = classProbs,
						 savePredictions = savePredictions
						 )
	}

	if (method == "nnet") {
		svm.tune <-
			train(
				  x = X,
				  y = Y,
				  method = method,
				  metric = metric,
				  trControl = ctrl,
				  tuneLength = tuneLength,
				  MaxNWts = MaxNWts,
				  preProcess = pp
				  )
	} else if (method == "caretEnsemble")  {
		library(caretEnsemble)
		#methodList = c("svmRadial", "rf", "xgbTree")
		set.seed(123)
		#Y = as.numeric(Y)
		dat = data.frame(cbind(X, response = Y), check.names = F)
		tuneList = list()
		for (ml in ensemble.ml.algorithm) {
			if (ml == "nnet") {
				tuneList[[length(tuneList) + 1]] = caretModelSpec(
																  method = ml,
																  trace = F,
																  MaxNWts = MaxNWts
																  )
			} else if (ml == "glmnet") {
				enetGrid = expand.grid(.alpha = seq(.05, 1, length = 5),
									   .lambda = c((1:5) / 10))
				tuneList[[length(tuneList) + 1]] = caretModelSpec(method = ml, "tuneGrid" = enetGrid)
			} else {
				tuneList[[length(tuneList) + 1]] = caretModelSpec(method = ml)
			}
		}

		model_list <- caretList(
								response ~ .,
								data = dat,
								trControl = ctrl,
								metric = metric,
								tuneList = tuneList,
								tuneLength = tuneLength,
								continue_on_fail = continue_on_fail,
								preProc = pp
								#methodList=ensemble.ml.algorithm
								)
		if (casecontrol & length(table(Y)) == 2) {
			svm.tune <-
				caretEnsemble(
							  model_list,
							  metric = metric,
							  trControl = trainControl(
													   allowParallel = allowParallel,
													   number = length(model_list),
													   summaryFunction = mysummaryFunction,
													   classProbs = classProbs
													   )
							  #trControl=my_control
							  )
		} else {
			my_model_list = list()
			for (n in names(model_list)) {
				#my_model_list[[n]] = model_list[[n]]
				ml.model = model_list[[n]]
				my_model_list[[n]] = predict(object = ml.model, X)
			}
			if (F) {
				class(my_model_list) <- "caretList"
				my_model_list2 = resamples(my_model_list)
				class(my_model_list2) <- "caretList"
				#!still not work
				svm.tune <-
					caretStack(my_model_list,
							   method = "multinom")
			}
			my_model_list = as.data.frame(my_model_list)
			svm.tune <-
				train(
					  my_model_list,
					  Y,
					  method = 'gbm',
					  trControl = trainControl(
											   allowParallel = allowParallel,
											   summaryFunction = mysummaryFunction,
											   classProbs = classProbs
											   )
					  )
		}
		#summary(greedy_ensemble)
		#svm.tune = greedy_ensemble

	} else if (method == "glmnet")  {
		enetGrid = expand.grid(.alpha = seq(.05, 1, length = 5),
							   .lambda = c((1:5) / 10))
		svm.tune <-
			train(
				  x = X,
				  y = Y,
				  method = method,
				  metric = metric,
				  trControl = ctrl,
				  tuneGrid = enetGrid,
				  tuneLength = tuneLength,
				  preProcess = pp
				  )
	} else {
		svm.tune <-
			train(
				  x = X,
				  y = Y,
				  method = method,
				  metric = metric,
				  trControl = ctrl,
				  tuneLength = tuneLength,
				  preProcess = pp
				  )
	}

	if (is.null(out_rdata)){
		return (svm.tune)
	} else {
		save(svm.tune, file = out_rdata)
	}

	return (svm.tune)
}
