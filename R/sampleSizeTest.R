sampleSizeTest <- function (# testing the relationship of ROC and sample size
    #only support casecontrol
    predictor.rank.f = NULL, # sorted predictor imp file, to specify predictors
    predictor.top.num = NULL, # top number of predictors to be used
    predictor.rank.f.header = T,
    CPUnum = 20,
    
    ml.algorithm = "svmLinear",
    ensemble.ml.algorithm = c("rf", "svmLinear", "glmnet", "knn"), #ML methods for greedy ensemble, if caretEnsemble is included in ml.algorithm 
    #for cv repeat times
    repeats = 2,
    #for tune length parameter
    tuneLength = 4,
    
    outdir = "FS_sample_size",
    train_out_rdata = "train.dat.rdata", #training set rdata, which will be used to evaluate sample size
    response = "response", #response header name in train.dat
    response.level = c("Cont", "Case"),
    pdffile = paste0(ml.algorithm, "_sampleSize.pdf"),
    
    casecontrol = T, #only casecontrol response is supported in curret version
    classProbs = F,
    metric = "ROC", # only "ROC" is supported
    pp = c("center", "scale"), #preProc / preProcess parameter, NULL, center, scale ... 
    mysummaryFunction = twoClassSummary,  # twoClassSummary or multiClassSummary
    validation_rdata = NULL,
    ...)
{
    if (!casecontrol | metric != "ROC") {
        stop ("Only casecontrol response and ROC metric were supported in current version. ")
    }
    
    library(caret)
    library(pROC) # plot the ROC curves
    
    library(doMC)
    registerDoMC(CPUnum)
    options(warn=-1)
    
    if (!file.exists(outdir)) {
        dir.create(outdir, recursive = T)
    }
    
    load(train_out_rdata)
    
    train.dat[, response] = as.factor(train.dat[, response])
    levels(train.dat[, response]) = response.level
    
    if (!is.null(predictor.rank.f)) {
        imp = read.delim(predictor.rank.f, predictor.rank.f.header)[, 1]
        if (!is.null(predictor.top.num)) {
            imp = imp[1:predictor.top.num]
        }
        train.dat.all = train.dat[, c(imp, response)]
    } else {
        train.dat.all = train.dat
    }
    
    if (!file.exists(outdir)) system(paste0("mkdir -p ", outdir))
    Y.bak = Y = train.dat.all$response
    case.i = which(Y == levels(Y)[2])
    cont.i = which(Y == levels(Y)[1])
    maxnum = ifelse(length(case.i) > length(cont.i), length(cont.i), length(case.i))
    if (maxnum <= 10) {
        stop ("At least 10 case or 10 control samples are required in train.dat.")
    }
    out = list()
    for (s in 10:maxnum) {
        message (paste0(2*s, " samples out of ", 2*maxnum, " ..."))
        X = train.dat.all[c(cont.i[1:s], case.i[1:s]),-ncol(train.dat.all)]
        Y = Y.bak[c(cont.i[1:s], case.i[1:s])]
        temp_train_in_rdata = paste0(outdir, "/", s, "_input.rdata")
        temp_train_out_rdata = paste0(outdir, "/", s, "_model.rdata")
        if (!file.exists(temp_train_in_rdata)) save(X, Y, file=temp_train_in_rdata)
        if (!file.exists(temp_train_out_rdata)) {
            fineTuning(
                in_rdata = temp_train_in_rdata,
                method = ml.algorithm,
                tuneLength = tuneLength,
                out_rdata = temp_train_out_rdata,
                repeats = repeats,
                ensemble.ml.algorithm = ensemble.ml.algorithm,
                savePredictions = "final",
                metric = metric,
                pp = pp, #preProc / preProcess parameter, NULL, center, scale ...
                continue_on_fail = T, #for caretList
                mysummaryFunction = mysummaryFunction, #twoClassSummary or multiClassSummary
                classProbs = classProbs,
                casecontrol = casecontrol,
                val.index.list.f = NULL
            )
        }
        load(temp_train_out_rdata)
        (model =  svm.tune)
        out[[length(out)+1]] = model
    }
    
    names(out) = 2*(10:maxnum)
    
    all.roc = c()
    if (ml.algorithm != "caretEnsemble") {
        for (i in 1:length(out)) {
            o = out[[i]]
            roc = max(o$results$ROC)
            all.roc = c(all.roc, roc)
        }
    } else {
        for (i in 1:length(out)) {
            roc = out[[i]]$ens_model$results
            all.roc = c(all.roc, roc$ROC)
        }
    }
    
    dat = cbind(ROC=all.roc, Sample=2*(10:maxnum))
    dat = as.data.frame(dat)
    
    require(RColorBrewer)
    cols <- brewer.pal(8, "Set1")[2]
    library(ggplot2)
    p = ggplot(dat,
               aes(Sample, ROC, color=cols[1])) +
        geom_line(size = 1, alpha = 0.8)+
        scale_color_manual(values = cols) +
        theme(legend.position = "none") +
        labs(title= NULL,
             x = "Sample size",
             y = "auROC"
        )
    pdf(pdffile, 3,  3, family = "ArialMT")
    print (p)
    dev.off()
    
    options(warn=0)
}