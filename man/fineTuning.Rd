\name{fineTuning}
\alias{fineTuning}

\title{
    using caret to perform fine tuning
}

\description{
    Find the best models, and produce models 
}

\usage{
 fineTuning(repeats, tuneLength, train_out_rdata)
}

\arguments{
   \item{in_rdata}{temprory input training rdata}
   \item{method}{machine learning method}
   \item{tuneLength}{tuneLength caret parameter}
   \item{out_rdata}{output rdata name of the model}
   \item{repeats}{repeat caret parameter}
   \item{val.index.list.f}{previously generated sample validation index rdata file}
}

\author{
 Xinyu Zhang <xinyu.zhang@yale.edu>
 Ying Hu <yhu@mail.nih.gov>
}

\references{
  ##
}

\examples{
}

